package com.nostratech.project.util;

import lombok.extern.slf4j.Slf4j;

import javax.naming.Context;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import java.util.Properties;

@Slf4j
public class LDAPUtil
{
    public static DirContext connectIntoLDAP(boolean prdsentul)
    {
        try
        {
            Properties prop = new Properties();
            prop.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");

            if(prdsentul == true)
            {
                prop.put(Context.PROVIDER_URL, "ldap://10.0.63.96:3060");
                prop.put(Context.SECURITY_PRINCIPAL, "cn=orcladmin");
                prop.put(Context.SECURITY_AUTHENTICATION, "simple");
                prop.put(Context.SECURITY_CREDENTIALS, "TelkomSDP2017");
            }

            else
            {
                prop.put(Context.PROVIDER_URL, "ldap://10.0.56.110:3060");
                prop.put(Context.SECURITY_PRINCIPAL, "cn=orcladmin");
                prop.put(Context.SECURITY_AUTHENTICATION, "simple");
                prop.put(Context.SECURITY_CREDENTIALS, "t3lk0mSOAB4t3r3");
            }

            log.info("=====Connecting Into LDAP=====");
            DirContext context = new InitialDirContext(prop);
            log.info("=====Connected Into LDAP=====");

            return context;
        }

        catch(Exception e)
        {
            log.info("Error couldn't connect LDAP, cause "+e.getMessage());
            e.printStackTrace();
        }

        return null;
    }
}