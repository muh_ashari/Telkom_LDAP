package com.nostratech.project.controller;

import com.nostratech.project.persistence.service.MigrasiUserService;
import com.nostratech.project.persistence.service.MigrasiUserService3;
import com.nostratech.project.persistence.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/ldap")
public class LDAPController
{
    @Autowired
    MigrasiUserService ldapServicePRDSentul;

    @Autowired
    MigrasiUserService3 service3;

    @RequestMapping
    (
        value = "/get-all",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    public ResponseEntity<ResultVO> getAll(@RequestParam(value = "jumlah", required = true) long jumlah)
    {
        AbstractRequestHandler handler = new AbstractRequestHandler()
        {
            @Override
            public Object processRequest()
            {
                return service3.getAttributesAndBaseDN(jumlah);
            }
        };
        return handler.getResult();
    }

    @RequestMapping
    (
        value = "/insert-all",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    public ResponseEntity<ResultVO> insertAll(@RequestParam(value = "jumlah", required = true) long jumlah)
    {
        AbstractRequestHandler handler = new AbstractRequestHandler()
        {
            @Override
            public Object processRequest()
            {
                return service3.insertUser(jumlah);
            }
        };
        return handler.getResult();
    }

    @RequestMapping
    (
        value = "/get-all-ldap-user-prd-sentul",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    public ResponseEntity<ResultVO> getAllUserPRDSentul()
    {
        AbstractRequestHandler handler = new AbstractRequestHandler()
        {
            @Override
            public Object processRequest()
            {
                return ldapServicePRDSentul.getAllUserAndAttributeAndObjectClassLDAP(true, 100, null);
            }
        };
        return handler.getResult();
    }

    @RequestMapping
    (
        value = "/insert-ldap-user-prd-sentul",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    public ResponseEntity<ResultVO> insertIntoPRDSentul()
    {
        AbstractRequestHandler handler = new AbstractRequestHandler()
        {
            @Override
            public Object processRequest()
            {
                return ldapServicePRDSentul.insertUser( 350000);
            }
        };
        return handler.getResult();
    }

    @RequestMapping
    (
        value = "/get-count-basedn-ods",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    public ResponseEntity<ResultVO> getCountBaseDNODS()
    {
        AbstractRequestHandler handler = new AbstractRequestHandler()
        {
            @Override
            public Object processRequest()
            {
                return ldapServicePRDSentul.countBaseDN();
            }
        };
        return handler.getResult();
    }

    @RequestMapping
    (
        value = "/get-basedn-prd-sentul",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    public ResponseEntity<ResultVO> getBaseDNPRDSentul()
    {
        AbstractRequestHandler handler = new AbstractRequestHandler()
        {
            @Override
            public Object processRequest()
            {
                return ldapServicePRDSentul.getAllBaseDN(true, 100);
            }
        };
        return handler.getResult();
    }

    @RequestMapping
    (
        value = "/get-basedn-ods",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    public ResponseEntity<ResultVO> getBaseDNODS()
    {
        AbstractRequestHandler handler = new AbstractRequestHandler()
        {
            @Override
            public Object processRequest()
            {
                return ldapServicePRDSentul.getAllBaseDN(false, 1000000);
            }
        };
        return handler.getResult();
    }

    @RequestMapping
    (
        value = "/get-all-ldap-user-prd-ods",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    public ResponseEntity<ResultVO> getAllUserODSSentul()
    {
        AbstractRequestHandler handler = new AbstractRequestHandler()
        {
            @Override
            public Object processRequest()
            {
                return ldapServicePRDSentul.readFromCSV();
            }
        };
        return handler.getResult();
    }

    @RequestMapping
    (
        value = "/get-distinct-ods-basedn",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    public ResponseEntity<ResultVO> getDistinctODSBaseDN()
    {
        AbstractRequestHandler handler = new AbstractRequestHandler()
        {
            @Override
            public Object processRequest()
            {
                return ldapServicePRDSentul.distinctBaseDN(350000);
            }
        };
        return handler.getResult();
    }
}