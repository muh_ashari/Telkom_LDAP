package com.nostratech.project.persistence.service;

import com.nostratech.project.persistence.vo.ObjectClassVO;
import com.nostratech.project.persistence.vo.UserOULDAPVO;
import com.nostratech.project.persistence.vo.UserOUParentVO;
import com.nostratech.project.util.LDAPUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.naming.NamingEnumeration;
import javax.naming.directory.*;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
/**
 * @author : Ari
 */
public class MigrasiUserService2
{
    public List<UserOULDAPVO> getAllUserAndAttributeAndObjectOUClassLDAP (boolean paramPRDSentul) {

        try
        {
            List<UserOULDAPVO> userOULDAPVOList = new ArrayList<UserOULDAPVO>();
            DirContext context = LDAPUtil.connectIntoLDAP(paramPRDSentul);

            SearchControls controls = new SearchControls();
            controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            controls.setReturningAttributes(new String[] {"objectClass","ou"});

            NamingEnumeration<SearchResult> orgUnit = context.search("cn=Users,dc=telkom,dc=co,dc=id", "(objectClass=organizationalUnit)", controls);
            log.info("=====Fetching Data, Please wait=====");

            while(orgUnit.hasMoreElements() == true)
            {
                Attributes atts = orgUnit.next().getAttributes();
                UserOULDAPVO ldapvo = new UserOULDAPVO();
                List<ObjectClassVO> objectClassVOList = new ArrayList<>();

                Attribute atr = atts.get("objectClass");
                for(NamingEnumeration ne = atr.getAll(); ne.hasMoreElements();)
                    objectClassVOList.add(new ObjectClassVO("" + ne.next()));


                if(atts.get("ou") == null)
                    ldapvo.setOu(null);
                else
                    ldapvo.setOu(atts.get("ou").get().toString());


                ldapvo.setObjectClassVO(objectClassVOList);
                userOULDAPVOList.add(ldapvo);
            }

            log.info("=====Finish Fetch All Data=====");
            log.info("Total Get All orclUser = ["+userOULDAPVOList.size()+"]");

            return userOULDAPVOList;
        }

        catch(Exception e)
        {
            log.info("Error couldn't get all user LDAP, cause "+e.getMessage());
            e.printStackTrace();
        }

        return null;

    }

    public List<UserOUParentVO> getAllUserAndAttributeAndObjectOUParentClassLDAP (boolean paramPRDSentul) {

        try {
            List<UserOUParentVO> userOUParentLDAPVOList = new ArrayList<UserOUParentVO>();

            DirContext context = LDAPUtil.connectIntoLDAP(paramPRDSentul);
            SearchControls controls = new SearchControls();
            controls.setSearchScope(SearchControls.ONELEVEL_SCOPE);

            controls.setReturningAttributes(new String[]{"objectClass", "ou"});
            NamingEnumeration<SearchResult> orgUnit = context.search("cn=Users,dc=telkom,dc=co,dc=id", "ou=*", controls);
            log.info("=====Fetching Data, Please wait=====");

            while (orgUnit.hasMoreElements() == true) {
                Attributes atts = orgUnit.next().getAttributes();
                UserOUParentVO ldapvo = new UserOUParentVO();
                List<ObjectClassVO> objectClassVOList = new ArrayList<>();

                Attribute atr = atts.get("objectClass");
                for (NamingEnumeration ne = atr.getAll(); ne.hasMoreElements(); )
                    objectClassVOList.add(new ObjectClassVO("" + ne.next()));

                if (atts.get("ou") == null)
                    ldapvo.setOuParent(null);
                else
                    ldapvo.setOuParent(atts.get("ou").get().toString());

                ldapvo.setObjectClassVO(objectClassVOList);
                userOUParentLDAPVOList.add(ldapvo);
            }

            log.info("=====Finish Fetch All Data=====");
            log.info("Total Get All orclUser = [" + userOUParentLDAPVOList.size() + "]");

            return userOUParentLDAPVOList;


        } catch (Exception e) {
            log.info("Error couldn't get all user LDAP, cause " + e.getMessage());
            e.printStackTrace();
        }

        return null;

    }

    public Boolean insertUserOUParentIntoODS()
    {
        try
        {
            DirContext context = LDAPUtil.connectIntoLDAP(true);
            List<UserOUParentVO> listODS = this.getAllUserAndAttributeAndObjectOUParentClassLDAP(false);

            log.info("=====Proses Find and Insert Users Please Wait!=====");
            for(int i=0; i<listODS.size(); i++)
            {

                //Searching for the list of user
                SearchControls sc = new SearchControls();
                sc.setSearchScope(SearchControls.ONELEVEL_SCOPE);
                NamingEnumeration ne = context.search("cn=Users,dc=telkom,dc=co,dc=id","(&(ou="+listODS.get(i).getOuParent()+"))", sc);

                //Checking if the anymore element in the code
                if(ne.hasMoreElements() == false)
                {
                    BasicAttributes basicAttribute = new BasicAttributes();
                    Attribute attribute = new BasicAttribute("objectclass");

                    int jumlahObjectVO = listODS.get(i).getObjectClassVO().size();
                    for(int j=0; j<jumlahObjectVO; j++) attribute.add(listODS.get(i).getObjectClassVO().get(j).getObjClass());

                    basicAttribute.put(attribute);

                    if(listODS.get(i).getOuParent()  != null) basicAttribute.put("ou", listODS.get(i).getOuParent());


                        context.createSubcontext("ldap://10.0.63.97:3060/ou=" + listODS.get(i).getOuParent() + ",cn=Users,dc=telkom,dc=co,dc=id", basicAttribute);
                        log.info("ou="+listODS.get(i).getOuParent()+" Has been Added");


                    log.info("=====Success insert user into LDAP User = ["+listODS.get(i).getOuParent()+"]["+i+"]======");
                }
            }
            log.info("=====Selesai Semua Proses Find and Insert User=====");

            return true;
        }

        catch(Exception e)
        {
            log.info("Error couldn't Insert Usert into LDAP, cause "+e.getMessage());
            e.printStackTrace();
        }

        return false;
    }


    public Boolean insertUserOUintoODS() {
        try {
            DirContext context = LDAPUtil.connectIntoLDAP(true);
            List<UserOUParentVO> listODS = this.getAllUserAndAttributeAndObjectOUParentClassLDAP(false);

            log.info("=====Proses Find and Insert Users Please Wait!=====");
            for (int i = 0; i < listODS.size(); i++) {
                SearchControls sc = new SearchControls();
                sc.setSearchScope(SearchControls.ONELEVEL_SCOPE);

                NamingEnumeration ne = context.search("cn=Users,dc=telkom,dc=co,dc=id", "(&(ou=" + listODS.get(i).getOuParent() + "))", sc);


            }

        } catch (Exception e) {
            log.info("Error couldn't Insert Usert into LDAP, cause " + e.getMessage());
            e.printStackTrace();
        }

        return false;
    }



}